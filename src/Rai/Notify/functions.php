<?php

if (!function_exists('notify')) {
    /**
     * Return app instance of Notify.
     * 
     * @return Rai\Notify\Notifier
     */
    function notify() {
        return app('notify');
    }
}
